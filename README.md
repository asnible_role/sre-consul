# Ansible Role: Consul

An Ansible Role that install or upgrade Consul to Linux.

## Requirements

None.

## Role Variables

Available variables are listed below, along with default values:
```yaml
repo: /etc/apk/
```

## Dependencies

None.

## Example Playbook

```yaml
- hosts: all
  become: yes
  roles:
    - sre-consul
```

## Example inventory
```ini
[nodes]
node-1  ansible_host=192.168.1.61  server=true
node-2  ansible_host=192.168.1.254 server=false
node-3  ansible_host=192.168.1.252 server=false

[nodes:vars]
ansible_user=anton
```

## License

MIT / BSD

## Author Information

This role was created in 2020 by [Nm@w](https://gitlab.com/nmaw).


https://releases.hashicorp.com/consul/1.7.2/consul_1.7.2_linux_amd64.zip